from collections.abc import Iterable

from aoc.day05 import a


def count_points_with_overlaps(
    vent_lines: Iterable[a.VentLine],
) -> int:
    return sum(1 for _, count in a.count_vents(vent_lines).items() if count > 1)


if __name__ == "__main__":
    import sys

    import typer

    def main():
        vent_lines = tuple(a.parse_vent_line_input(line) for line in sys.stdin)
        print(
            f"There are {count_points_with_overlaps(vent_lines)} points where two vent lines overlap."
        )

    typer.run(main)
