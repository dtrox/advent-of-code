from collections.abc import Iterable
from dataclasses import dataclass
import re


@dataclass
class VentLine:
    x1: int
    y1: int
    x2: int
    y2: int

    @property
    def vents(self) -> tuple[tuple[int, int], ...]:
        xstep = -1 if self.x2 < self.x1 else 1
        xvals = tuple(range(self.x1, self.x2 + xstep, xstep))
        ystep = -1 if self.y2 < self.y1 else 1
        yvals = tuple(range(self.y1, self.y2 + ystep, ystep))
        if len(xvals) == 1:
            xvals = xvals * len(yvals)
        if len(yvals) == 1:
            yvals = yvals * len(xvals)
        return tuple(zip(xvals, yvals))


def count_vents(vent_lines: Iterable[VentLine]) -> dict[tuple[int, int], int]:
    vent_counts = {}
    for line in vent_lines:
        for vent in line.vents:
            if vent in vent_counts:
                vent_counts[vent] += 1
            else:
                vent_counts[vent] = 1
    return vent_counts


def count_points_with_overlaps_excluding_diagonals(
    vent_lines: Iterable[VentLine],
) -> int:
    vent_counts = count_vents(
        vl for vl in vent_lines if vl.x1 == vl.x2 or vl.y1 == vl.y2
    )
    return sum(1 for _, count in vent_counts.items() if count > 1)


_input_pattern = re.compile(r"^(\d+),(\d+) -> (\d+),(\d+)\n$")


def parse_vent_line_input(line: str) -> VentLine:
    match = _input_pattern.match(line)
    if match is None:
        raise ValueError("invalid line")
    return VentLine(*(int(match.group(i)) for i in range(1, 5)))


if __name__ == "__main__":
    import sys

    import typer

    def main():
        vent_lines = tuple(parse_vent_line_input(line) for line in sys.stdin)
        print(
            f"There are {count_points_with_overlaps_excluding_diagonals(vent_lines)} points where two vent lines overlap."
        )

    typer.run(main)
