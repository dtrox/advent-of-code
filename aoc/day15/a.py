from dataclasses import dataclass
import math
from queue import PriorityQueue
import sys
from typing import Iterator


Node = tuple[int, int]


UNDEFINED: Node = (-1, -1)


def neighbors(node: Node, x_max: int, y_max: int) -> Iterator[Node]:
    if node[0] - 1 > 0:
        yield (node[0] - 1, node[1])
    if node[0] + 1 < x_max:
        yield (node[0] + 1, node[1])
    if node[1] - 1 > 0:
        yield (node[0], node[1] - 1)
    if node[1] + 1 < y_max:
        yield (node[0], node[1] + 1)


@dataclass
class Route:
    path: tuple[Node]
    risk: float


def optimal_cavern_route(risk_levels: list[list[int]]):
    """uniform cost search"""
    shape = (len(risk_levels[0]), len(risk_levels))
    costs = {
        (x, y): cost for y, row in enumerate(risk_levels) for x, cost in enumerate(row)
    }
    distances: dict[Node, float] = {
        (x, y): math.inf for y in range(shape[1]) for x in range(shape[0])
    }
    previous: dict[Node, Node] = {
        (x, y): UNDEFINED for y in range(shape[1]) for x in range(shape[0])
    }

    start = (0, 0)
    distances[start] = 0.0

    queue = PriorityQueue()
    queue.put((0, start))

    goal = (shape[0] - 1, shape[1] - 1)

    while not queue.empty():
        _, node = queue.get()
        if node == goal:
            break

        for neighbor in neighbors(node, shape[0], shape[1]):
            alt_dist = distances[node] + costs[neighbor]
            if alt_dist < distances[neighbor]:
                previous[neighbor] = node
                distances[neighbor] = alt_dist
                queue.put((distances[neighbor], neighbor))

    curr = goal
    path = [curr]
    while previous[curr] != UNDEFINED:
        curr = previous[curr]
        path.append(curr)

    return Route(tuple(reversed(path)), distances[goal])


def main():
    risk_levels = [[int(v) for v in line.strip()] for line in sys.stdin]
    print("\n".join(" ".join(str(v) for v in row) for row in risk_levels))

    route = optimal_cavern_route(risk_levels)
    print(route)


if __name__ == "__main__":
    main()
