import sys

from aoc.day15 import a


def calculate_new_risk_levels(risk_levels: list[list[int]], tiles: int = 5):
    shape = (len(risk_levels[0]), len(risk_levels))
    new = []
    for tile_y in range(0, tiles):
        for _ in range(len(risk_levels)):
            new.append([])
        for tile_x in range(0, tiles):
            for orig_y in range(shape[1]):
                for orig_x in range(shape[0]):
                    y = orig_y + tile_y * shape[1]
                    x = orig_x + tile_x * shape[0]
                    new[y].append(risk_levels[orig_y][orig_x] + (tile_x + tile_y))
                    if len(new[y]) > len(risk_levels) * tiles:
                        raise ValueError()
                    if new[y][x] > 9:
                        new[y][x] = new[y][x] - 9
    return new


def main():
    risk_levels = [[int(v) for v in line.strip()] for line in sys.stdin]

    risk_levels = calculate_new_risk_levels(risk_levels)
    print("\n".join(" ".join(str(v) for v in row) for row in risk_levels))

    route = a.optimal_cavern_route(risk_levels)
    print(route)


if __name__ == "__main__":
    main()
