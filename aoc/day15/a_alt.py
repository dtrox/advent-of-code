from collections.abc import Iterator, Iterable
import copy
import curses
from dataclasses import dataclass
import math
from queue import PriorityQueue
import queue
import sys
import time
from typing import Optional


UNDEFINED = (-1, -1)


def neighbors(
    node: tuple[int, int], x_max: int, y_max: int
) -> Iterator[tuple[int, int]]:
    if node[0] - 1 > 0:
        yield (node[0] - 1, node[1])
    if node[0] + 1 < x_max:
        yield (node[0] + 1, node[1])
    if node[1] - 1 > 0:
        yield (node[0], node[1] - 1)
    if node[1] + 1 < y_max:
        yield (node[0], node[1] + 1)


@dataclass
class Route:
    path: tuple["Node"]
    risk: float


@dataclass
class Node:
    x: int
    y: int
    cost: int
    distance: float = math.inf
    previous: Optional["Node"] = None

    @property
    def route(self):
        curr = self
        path = [curr]
        while curr.previous is not None:
            curr = curr.previous
            path.append(curr)
        return Route(tuple(reversed(path)), self.distance)


class SearchFail(Exception):
    pass


class UniformCostSearch(Iterable):
    def __init__(self, cost_map: list[list[int]]):
        self._shape = (len(cost_map[0]), len(cost_map))
        self._nodes = {
            (x, y): Node(x, y, cost)
            for y, row in enumerate(cost_map)
            for x, cost in enumerate(row)
        }
        self._queue = PriorityQueue()
        start = (0, 0)
        self._nodes[start].distance = 0.0
        self._queue.put((0, start))
        self._goal = (self._shape[0] - 1, self._shape[1] - 1)

    def __iter__(self) -> Iterator[Node]:
        while True:
            try:
                _, node = self._queue.get()
            except queue.Empty:
                raise SearchFail("balls")

            if node == self._goal:
                break

            for neighbor in neighbors(node, self._shape[0], self._shape[1]):
                node_ = self._nodes[node]
                neigh_ = self._nodes[neighbor]
                alt_dist = node_.distance + neigh_.cost
                if alt_dist < neigh_.distance:
                    neigh_.previous = self._nodes[node]
                    neigh_.distance = alt_dist
                    self._queue.put((neigh_.distance, neighbor))

            yield copy.copy(self._nodes[node])

        yield copy.copy(self._nodes[self._goal])


DELAY = 0.1


def main(stdscr: curses.window):
    stdscr.clear()

    risk_levels = [[int(v) for v in line.strip()] for line in sys.stdin]

    results = list(UniformCostSearch(risk_levels))

    for node in results:
        stdscr.addch(node.y, node.x, f"{node.cost}")
        stdscr.refresh()
        time.sleep(DELAY)

    for node in results[-1].route.path:
        stdscr.addch(node.y, node.x, "█")
        stdscr.refresh()
        time.sleep(DELAY)

    stdscr.addstr(str(results[-1].distance))
    stdscr.refresh()

    while True:
        time.sleep(1)


if __name__ == "__main__":
    curses.wrapper(main)
