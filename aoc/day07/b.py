from collections.abc import Iterable
from functools import cache
import math
import statistics

from aoc.day06.a import read_initial_state


@cache
def fuel_cost(distance: int) -> int:
    return int((2 + distance - 1) * distance / 2)


def solve_optimal_position(crabs: Iterable[int]) -> tuple[int, int]:
    crabs = tuple(crabs)
    mean = statistics.mean(crabs)
    return min(
        (
            (m, sum(fuel_cost(abs(p - m)) for p in crabs))
            for m in (math.floor(mean), math.ceil(mean))
        ),
        key=lambda t: t[1],
    )


if __name__ == "__main__":
    import typer

    def main(
        crabs_input: str,
    ):
        try:
            crabs = read_initial_state(crabs_input)
        except:
            typer.echo("could not read initial state", err=True)
            raise typer.Exit(1)

        optimal_position, fuel_cost = solve_optimal_position(crabs)
        typer.echo(
            f"The optimal crab position is {optimal_position}, costing {fuel_cost} fuel."
        )

    typer.run(main)
