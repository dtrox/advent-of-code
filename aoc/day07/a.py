from collections.abc import Iterable
import statistics

from aoc.day06.a import read_initial_state


def solve_optimal_position(crabs: Iterable[int]) -> tuple[int, int]:
    crabs = tuple(crabs)
    position = int(statistics.median(crabs))
    cost = sum(abs(p - position) for p in crabs)
    return position, cost


if __name__ == "__main__":
    import typer

    def main(
        crabs_input: str,
    ):
        try:
            crabs = read_initial_state(crabs_input)
        except:
            typer.echo("could not read initial state", err=True)
            raise typer.Exit(1)

        optimal_position, fuel_cost = solve_optimal_position(crabs)
        typer.echo(
            f"The optimal crab position is {optimal_position}, costing {fuel_cost} fuel."
        )

    typer.run(main)
