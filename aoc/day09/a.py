import sys

import typer


def low_points(heightmap: list[list[int]]) -> list[int]:
    length = len(heightmap)
    width = len(heightmap[0])

    def is_low_point(row: int, col: int) -> bool:
        adjacent = (
            (r, c)
            for r, c in ((row - 1, col), (row + 1, col), (row, col - 1), (row, col + 1))
            if r >= 0 and c >= 0 and r < length and c < width
        )
        return all(heightmap[row][col] < heightmap[r][c] for r, c in adjacent)

    return [
        heightmap[r][c]
        for r in range(length)
        for c in range(width)
        if is_low_point(r, c)
    ]


def risk_level(heightmap: list[list[int]]) -> int:
    return sum(1 + h for h in low_points(heightmap))


def parse_heightmap(data: str) -> list[list[int]]:
    return [list(map(int, l)) for l in data.splitlines()]


def main():
    data = sys.stdin.read()
    heightmap = parse_heightmap(data)
    typer.echo(f"The total risk level is {risk_level(heightmap)}")


if __name__ == "__main__":
    main()
