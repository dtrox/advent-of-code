from functools import reduce
import logging
from operator import mul
import sys

import typer

from aoc.day09 import a


def adjacent(loc: tuple[int, int], other: tuple[int, int]) -> bool:
    return abs(loc[0] - other[0]) + abs(loc[1] - other[1]) <= 1


class Basin(frozenset[tuple[int, int]]):
    def adjacent(self, other: "Basin") -> bool:
        return any(adjacent(loc, other_loc) for loc in self for other_loc in other)


def explore_basins(heightmap: list[list[int]]) -> list[Basin]:
    length = len(heightmap)
    width = len(heightmap[0])

    basin_locs = [
        (r, c) for r in range(length) for c in range(width) if heightmap[r][c] != 9
    ]
    basin_locs = sorted(basin_locs)

    basins = [Basin({loc}) for loc in basin_locs]
    logging.info(f"Exploring {len(basins)} potential basins...")

    while True:
        new_basins: list[Basin] = []
        for basin in basins:
            for idx, other in enumerate(new_basins):
                if basin.adjacent(other):
                    new_basins[idx] = Basin(basin | other)
                    break
            else:
                new_basins.append(basin)
        if len(new_basins) == len(basins):
            break
        logging.info(
            f"Reduced basin exploration to {len(new_basins)} potential basins..."
        )
        basins = new_basins

    return basins


def main():
    data = sys.stdin.read()
    heightmap = a.parse_heightmap(data)
    basins = explore_basins(heightmap)
    big_3 = sorted((len(b) for b in basins), reverse=True)[:3]
    typer.echo(
        f"There are {len(basins)} basins. The three biggest have sizes {big_3}. Multiplied together is {reduce(mul, big_3)}"
    )


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
