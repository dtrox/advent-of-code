import sys


class Paper:
    def __init__(self, shape: tuple[int, int]):
        self.shape = shape
        self.data = [[0] * shape[0] for _ in range(shape[1])]

    def __getitem__(self, loc: tuple[int, int]):
        return self.data[loc[1]][loc[0]]

    def __str__(self):
        return "\n".join(
            " ".join(str(self.data[y][x]) for x in range(self.shape[0]))
            for y in range(self.shape[1])
        )

    def __iter__(self):
        return (v for x in self.data for v in x)

    def mark(self, loc: tuple[int, int]):
        self.data[loc[1]][loc[0]] = 1

    def fold_up(self, idx: int):
        paper = Paper((self.shape[0], idx))
        offset = self.shape[1] - idx * 2
        print(offset)
        for y in range(paper.shape[1]):
            for x in range(paper.shape[0]):
                if self[(x, y)]:
                    paper.mark((x, y))
                match = self.shape[1] - (y + offset)
                if match >= 0 and match < self.shape[1] and self[(x, match)]:
                    paper.mark((x, y))
        return paper

    def fold_left(self, idx: int):
        paper = Paper((idx, self.shape[1]))
        offset = self.shape[0] - idx * 2
        print(offset)
        for y in range(paper.shape[1]):
            for x in range(paper.shape[0]):
                if self[(x, y)]:
                    paper.mark((x, y))
                match = self.shape[0] - (x + offset)
                if match >= 0 and match < self.shape[0] and self[(match, y)]:
                    paper.mark((x, y))
        return paper


def main():
    marks = []
    for line in sys.stdin:
        if not line.strip():
            break
        x, y = line.strip().split(",")
        marks.append((int(x), int(y)))

    shape = (max(m[0] for m in marks) + 1, max(m[1] for m in marks) + 1)
    print(shape)
    paper = Paper(shape)
    for mark in marks:
        paper.mark(mark)

    for line in sys.stdin:
        print(line)
        data = line.strip()[11:].split("=")
        axis = data[0]
        idx = int(data[1])
        if axis == "x":
            paper = paper.fold_left(idx)
        else:
            paper = paper.fold_up(idx)

    print(paper)


if __name__ == "__main__":
    main()
