"""Increment number of fish per spawn cycle in 2 counters."""

from collections import Counter
import itertools

from aoc.day06.benchmark.bench import SimulationConfig


def counter_simulator(config: SimulationConfig) -> int:
    incubator = Counter()
    school = Counter(config.init_state)

    for cycle in range(config.num_cycles):
        spawn_cycle = cycle % config.inverse_growth_rate
        new_fish_spawn_cycle = (
            spawn_cycle + config.wait_period
        ) % config.inverse_growth_rate
        incubator[new_fish_spawn_cycle] += school[spawn_cycle]
        school[spawn_cycle] += incubator.pop(spawn_cycle, 0)

    return sum(itertools.chain(*(school.values(), incubator.values())))
