"""Rotate indexes and increment a list of fish counts."""

from collections import Counter

from aoc.day06.benchmark.bench import SimulationConfig


def modulo_simulator(config: SimulationConfig) -> int:
    init_counts = Counter(config.init_state)
    spawn_cycles = config.inverse_growth_rate + config.wait_period
    school = [init_counts[spawn_cycle] for spawn_cycle in range(spawn_cycles)]

    for cycle in range(config.num_cycles):
        current_index = cycle % spawn_cycles
        new_fish_index = (cycle + config.inverse_growth_rate) % spawn_cycles
        school[new_fish_index] += school[current_index]

    return sum(school)
