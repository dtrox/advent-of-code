from enum import Enum
import typer

from aoc.day06.a import read_initial_state
from aoc.day06.benchmark import bench
from aoc.day06.benchmark.counter import counter_simulator
from aoc.day06.benchmark.deque import deque_simulator
from aoc.day06.benchmark.list import list_simulator
from aoc.day06.benchmark.matrix import matrix_simulator
from aoc.day06.benchmark.modulo import modulo_simulator
from aoc.day06.benchmark.naive import naive_simulator
from aoc.day06.benchmark.recursive import recursive_simulator


class FormatChoice(str, Enum):
    text = "text"
    csv = "csv"


def main(
    init_state_input: str,
    num_cycles: list[int] = typer.Option(lambda: [18], "--num-cycles", "-n"),
    inverse_growth_rate: int = 7,
    wait_period: int = 2,
    repetitions: int = 10000,
    format: FormatChoice = FormatChoice.text,
):
    try:
        init_state = read_initial_state(init_state_input)
    except:
        typer.echo("could not read initial state", err=True)
        raise typer.Exit(1)

    for cycles in num_cycles:
        config = bench.SimulationConfig(
            init_state, cycles, inverse_growth_rate, wait_period
        )

        simulators = [
            counter_simulator,
            deque_simulator,
            list_simulator,
            matrix_simulator,
            modulo_simulator,
        ]

        if cycles <= 18:
            simulators.append(naive_simulator)
        else:
            typer.echo("excluding naive_simulator due to high num_cycles", err=True)

        if cycles <= 256:
            simulators.append(recursive_simulator)
        else:
            typer.echo("excluding recursive_simulator due to high num_cycles", err=True)

        results = [
            bench.run_test(simulator, config, repetitions) for simulator in simulators
        ]

        if format == FormatChoice.text:
            typer.echo(bench.format_results(results))
        elif format == FormatChoice.csv:
            typer.echo(bench.format_results_csv(results))


if __name__ == "__main__":
    typer.run(main)
