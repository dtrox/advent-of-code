"""Rotate and increment a list of fish counts."""

from collections import Counter

from aoc.day06.benchmark.bench import SimulationConfig


def list_simulator(config: SimulationConfig) -> int:
    size = config.inverse_growth_rate + config.wait_period
    init_counts = Counter(config.init_state)
    school = [init_counts[spawn_cycle] for spawn_cycle in range(size)]

    for _ in range(config.num_cycles):
        spawns = school.pop(0)
        school[config.inverse_growth_rate - 1] += spawns
        school.append(spawns)

    return sum(school)
