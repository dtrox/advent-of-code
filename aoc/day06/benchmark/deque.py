"""Rotate and increment a deque of fish counts."""

from collections import Counter, deque

from aoc.day06.benchmark.bench import SimulationConfig


def deque_simulator(config: SimulationConfig) -> int:
    size = config.inverse_growth_rate + config.wait_period
    init_counts = Counter(config.init_state)
    school = deque(
        (init_counts[spawn_cycle] for spawn_cycle in range(size)),
        maxlen=size,
    )

    for _ in range(config.num_cycles):
        spawns = school.popleft()
        school[config.inverse_growth_rate - 1] += spawns
        school.append(spawns)

    return sum(school)
