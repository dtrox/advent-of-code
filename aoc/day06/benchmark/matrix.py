"""Simulate fish growth with a stochastic matrix."""

import numpy as np

from aoc.day06.benchmark.bench import SimulationConfig


def matrix_simulator(config: SimulationConfig) -> int:
    size = config.inverse_growth_rate + config.wait_period
    transition = []
    for i in range(size):
        state = [0] * (size)
        if i == size - 1:
            state[0] = 1
        else:
            state[i + 1] = 1
        if i == config.inverse_growth_rate - 1:
            state[0] = 1
        transition.append(state)

    init_state = list(config.init_state)
    init_array = np.array([init_state.count(i) for i in range(9)])

    matrix = np.array(transition)
    school = np.linalg.matrix_power(matrix, config.num_cycles).dot(init_array)
    return np.sum(school)
