"""Recursively count fish spawned by a given initial spawn, caching results for reuse."""

from functools import cache

from aoc.day06.benchmark.bench import SimulationConfig


@cache
def spawns(
    age: int, cycle: int, inverse_growth_rate: int = 7, wait_period: int = 2
) -> int:
    if cycle == 0:
        return 1

    if age == 0:
        repeat_spawns = spawns(
            inverse_growth_rate - 1, cycle - 1, inverse_growth_rate, wait_period
        )
        new_spawns = spawns(
            inverse_growth_rate + wait_period - 1,
            cycle - 1,
            inverse_growth_rate,
            wait_period,
        )
        return repeat_spawns + new_spawns

    return spawns(age - 1, cycle - 1)


def recursive_simulator(config: SimulationConfig) -> int:
    return sum(
        spawns(
            fish_age, config.num_cycles, config.inverse_growth_rate, config.wait_period
        )
        for fish_age in config.init_state
    )
