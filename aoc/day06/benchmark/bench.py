from collections.abc import Iterable
from dataclasses import dataclass
import functools
import timeit
import typing


@dataclass
class SimulationConfig:
    init_state: Iterable[int]
    num_cycles: int
    inverse_growth_rate: int = 7
    wait_period: int = 2


LanternfishSimulator = typing.Callable[[SimulationConfig], int]

TestResult = tuple[LanternfishSimulator, SimulationConfig, float]


def run_test(
    simulator: LanternfishSimulator,
    config: SimulationConfig,
    repetitions: int,
) -> TestResult:
    return (
        simulator,
        config,
        timeit.timeit(functools.partial(simulator, config), number=repetitions),
    )


def format_results(results: Iterable[TestResult]) -> str:
    results = list(results)
    name_spacing = max(len(f.__name__) for f, _, _ in results) + 4
    return "\n".join(
        f"{f.__name__.rjust(name_spacing)}  {t:2.4f} seconds" for f, _, t in results
    )


def format_results_csv(results: Iterable[TestResult]) -> str:
    return "\n".join(
        ",".join(
            map(
                str, (c.inverse_growth_rate, c.wait_period, c.num_cycles, f.__name__, t)
            )
        )
        for f, c, t in results
    )
