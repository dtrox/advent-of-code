"""Grow a list of fish timers exponentially, and run out of RAM."""

from aoc.day06.benchmark.bench import SimulationConfig


def naive_simulator(config: SimulationConfig) -> int:
    school = list(config.init_state)

    for _ in range(config.num_cycles):
        new_fish = 0
        for idx in range(len(school)):
            if school[idx] == 0:
                new_fish += 1
                school[idx] = config.inverse_growth_rate - 1
            else:
                school[idx] -= 1
        school.extend([config.inverse_growth_rate + config.wait_period - 1] * new_fish)

    return len(school)
