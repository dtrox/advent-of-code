from aoc.day06 import a


if __name__ == "__main__":
    import typer

    def main(
        init_state_input: str,
        num_cycles: int = 256,
        inverse_growth_rate: int = 7,
        wait_period: int = 2,
    ):
        try:
            init_state = a.read_initial_state(init_state_input)
        except:
            typer.echo("could not read initial state", err=True)
            raise typer.Exit(1)

        num_fish = a.simulate_lanternfish_school_growth(
            init_state,
            num_cycles,
            inverse_growth_rate=inverse_growth_rate,
            wait_period=wait_period,
        )
        typer.echo(f"After {num_cycles} days, there are a total of {num_fish} fish.")

    typer.run(main)
