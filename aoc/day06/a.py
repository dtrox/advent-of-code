from collections import Counter
from collections.abc import Iterable
import itertools
import logging
from pathlib import Path
import sys


def simulate_lanternfish_school_growth(
    init_state: Iterable[int],
    num_cycles: int,
    inverse_growth_rate: int = 7,
    wait_period: int = 2,
) -> int:
    """Simulate lanternfish school growth.

    Args:
        init_state: Initial state of the lanternfish school.
        num_cycles: Number of days of growth to simulate.
        inverse_growth_rate: Number of cycles before a lanternfish spawns a new lanternfish.
        wait_period: Number of cycles before a spawned lanternfish matures.

    Returns:
        Number of lanternfish in the school at simulation completion.
    """
    logger = logging.getLogger(__name__)
    incubator = Counter()
    school = Counter(init_state)
    logger.debug(f"Initial State: {school}")

    for cycle in range(num_cycles):
        spawn_cycle = cycle % inverse_growth_rate
        incubator[(spawn_cycle + wait_period) % inverse_growth_rate] += school[
            spawn_cycle
        ]
        school[spawn_cycle] += incubator[spawn_cycle]
        incubator[spawn_cycle] = 0
        logger.debug(
            f"Cycle: {cycle}; Spawn Cycle: {spawn_cycle}; School: {school}; Incubator: {incubator}"
        )

    return sum(itertools.chain(*(school.values(), incubator.values())))


def read_initial_state(input_: str) -> tuple[int]:
    """CLI helper to read and parse initial state from an input value.

    Provide "-" to read initial state from stdin.
    Provide a file path to read initial state from a file.
    Provide a comma delimited sequence of integers to provide initial state directly.
    """
    if input_ == "-":
        init_state_line = sys.stdin.readline()
    elif (filepath := Path(input_)).exists():
        if filepath.is_file():
            with open(input_) as fobj:
                init_state_line = fobj.readline()
        else:
            raise ValueError("invalid input file path: not a file")
    else:
        init_state_line = input_

    return tuple(int(x) for x in init_state_line.split(","))


if __name__ == "__main__":
    import typer

    def main(
        init_state_input: str,
        num_cycles: int = 80,
        inverse_growth_rate: int = 7,
        wait_period: int = 2,
    ):
        try:
            init_state = read_initial_state(init_state_input)
        except:
            typer.echo("could not read initial state", err=True)
            raise typer.Exit(1)

        num_fish = simulate_lanternfish_school_growth(
            init_state,
            num_cycles,
            inverse_growth_rate=inverse_growth_rate,
            wait_period=wait_period,
        )
        typer.echo(f"After {num_cycles} days, there are a total of {num_fish} fish.")

    typer.run(main)
