from collections import Counter
import itertools as it
import sys


def pairwise(iterable):
    a, b = it.tee(iterable)
    next(b, None)
    return zip(a, b)


def simulate_polymer_strength(
    template: str, rules: dict[str, str], iterations: int
) -> int:
    pairs = Counter("".join(p) for p in pairwise(template))
    elements = Counter(template)

    for _ in range(iterations):
        for pair, count in list(pairs.items()):
            if pair in rules:
                elements[rules[pair]] += count
                pairs[pair] -= count
                pairs[pair[0] + rules[pair]] += count
                pairs[rules[pair] + pair[1]] += count

    most_common = elements.most_common()
    return most_common[0][1] - most_common[-1][1]


def main():
    template = next(sys.stdin).strip()
    _ = next(sys.stdin)
    rules = {
        pair: insert for pair, insert in (l.strip().split(" -> ") for l in sys.stdin)
    }

    result = simulate_polymer_strength(template, rules, 40)
    print(result)


if __name__ == "__main__":
    main()
