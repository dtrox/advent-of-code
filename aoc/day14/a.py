from collections import Counter
import itertools as it
import sys


def pairwise(iterable):
    a, b = it.tee(iterable)
    next(b, None)
    return zip(a, b)


def polymerize(template: str, rules: dict[str, str]) -> str:
    result = []
    for pair in pairwise(template):
        result.append(f"{pair[0]}{rules[''.join(pair)]}")
    else:
        result.append(template[-1])
    return "".join(result)


def main():
    template = next(sys.stdin).strip()
    _ = next(sys.stdin)
    rules = {
        pair: insert for pair, insert in (l.strip().split(" -> ") for l in sys.stdin)
    }

    print(template)
    print(rules)

    result = template
    for _ in range(10):
        result = polymerize(result, rules)

    most_common = Counter(result).most_common()
    print(most_common[0][1] - most_common[-1][1])


if __name__ == "__main__":
    main()
