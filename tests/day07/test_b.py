from aoc.day07 import b


def test_fuel_cost():
    assert b.fuel_cost(11) == 66


def test_solve_optimal_position():
    crabs = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
    position, cost = b.solve_optimal_position(crabs)
    assert position == 5
    assert cost == 168
