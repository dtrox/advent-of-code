from aoc.day07 import a


def test_solve_optimal_position():
    crabs = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
    position, cost = a.solve_optimal_position(crabs)
    assert position == 2
    assert cost == 37
