from aoc.day06.benchmark.bench import SimulationConfig
from aoc.day06.benchmark.counter import counter_simulator
from aoc.day06.benchmark.deque import deque_simulator
from aoc.day06.benchmark.list import list_simulator
from aoc.day06.benchmark.matrix import matrix_simulator
from aoc.day06.benchmark.modulo import modulo_simulator
from aoc.day06.benchmark.naive import naive_simulator
from aoc.day06.benchmark.recursive import recursive_simulator


INIT_STATE = (3, 4, 3, 1, 2)
INVERSE_GROWTH_RATE = 7
WAIT_PERIOD = 2


TEST_18 = (
    SimulationConfig(
        init_state=INIT_STATE,
        num_cycles=18,
        inverse_growth_rate=INVERSE_GROWTH_RATE,
        wait_period=WAIT_PERIOD,
    ),
    26,
)


TEST_80 = (
    SimulationConfig(
        init_state=INIT_STATE,
        num_cycles=80,
        inverse_growth_rate=INVERSE_GROWTH_RATE,
        wait_period=WAIT_PERIOD,
    ),
    5934,
)


def test_counter_simulator():
    config, expected = TEST_80
    assert counter_simulator(config) == expected


def test_deque_simulator():
    config, expected = TEST_80
    assert deque_simulator(config) == expected


def test_list_simulator():
    config, expected = TEST_80
    assert list_simulator(config) == expected


def test_matrix_simulator():
    config, expected = TEST_80
    assert matrix_simulator(config) == expected


def test_modulo_simulator():
    config, expected = TEST_80
    assert modulo_simulator(config) == expected


def test_naive_simulator():
    config, expected = TEST_18
    assert naive_simulator(config) == expected


def test_recursive_simulator():
    config, expected = TEST_80
    assert recursive_simulator(config) == expected
