from functools import reduce
from operator import mul

from aoc.day09 import a, b


heightmap = a.parse_heightmap(
    """2199943210
3987894921
9856789892
8767896789
9899965678"""
)


def test_explore_basins():
    basins = b.explore_basins(heightmap)
    big_3 = sorted((len(b) for b in basins), reverse=True)[:3]
    assert reduce(mul, big_3) == 1134
