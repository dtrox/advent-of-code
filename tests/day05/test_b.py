from aoc.day05 import a, b


def test_count_points_with_overlaps():
    vent_lines = (
        a.VentLine(0, 9, 5, 9),
        a.VentLine(8, 0, 0, 8),
        a.VentLine(9, 4, 3, 4),
        a.VentLine(2, 2, 2, 1),
        a.VentLine(7, 0, 7, 4),
        a.VentLine(6, 4, 2, 0),
        a.VentLine(0, 9, 2, 9),
        a.VentLine(3, 4, 1, 4),
        a.VentLine(0, 0, 8, 8),
        a.VentLine(5, 5, 8, 2),
    )
    assert b.count_points_with_overlaps(vent_lines) == 12
