from aoc.day05 import a


def test_vertical_vents():
    vents = a.VentLine(1, 1, 1, 3).vents
    expected = ((1, 1), (1, 2), (1, 3))
    assert vents == expected


def test_horizontal_vents():
    vents = a.VentLine(9, 7, 7, 7).vents
    expected = ((9, 7), (8, 7), (7, 7))
    assert vents == expected


def test_diagonal_vents():
    vents = a.VentLine(1, 1, 3, 3).vents
    expected = ((1, 1), (2, 2), (3, 3))
    assert vents == expected

    vents = a.VentLine(9, 7, 7, 9).vents
    expected = ((9, 7), (8, 8), (7, 9))
    assert vents == expected


def test_count_vents():
    vent_lines = (
        a.VentLine(1, 1, 1, 3),
        a.VentLine(0, 2, 3, 2),
    )
    vent_counts = a.count_vents(vent_lines)
    assert set(vent_counts.items()) == set(
        {
            (1, 1): 1,
            (1, 2): 2,
            (1, 3): 1,
            (0, 2): 1,
            (2, 2): 1,
            (3, 2): 1,
        }.items()
    )


def test_count_points_with_overlaps_excluding_diagonals():
    vent_lines = (
        a.VentLine(0, 9, 5, 9),
        a.VentLine(8, 0, 0, 8),
        a.VentLine(9, 4, 3, 4),
        a.VentLine(2, 2, 2, 1),
        a.VentLine(7, 0, 7, 4),
        a.VentLine(6, 4, 2, 0),
        a.VentLine(0, 9, 2, 9),
        a.VentLine(3, 4, 1, 4),
        a.VentLine(0, 0, 8, 8),
        a.VentLine(5, 5, 8, 2),
    )
    assert a.count_points_with_overlaps_excluding_diagonals(vent_lines) == 5


def test_parse_vent_line_input():
    example = "1337,42 -> 256,11024\n"
    assert a.parse_vent_line_input(example) == a.VentLine(1337, 42, 256, 11024)
